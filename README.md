# October CMS - NativeDroid II



![nativeDroid2](http://dev.bisdev.space/themes/october-theme-native-droid/assets/images/header-screenshot.png)

A Theme for October CMS(https://octobercms.com/) inspired by JQuery Mobile / NativeDroid II(http://www.nativedroid.godesign.ch/material/)

# Install

  **git**

```git clone https://dyoungwd@bitbucket.org/dyoungwd/october-theme-native-droid.git```



# Demo & [Documentation](http://nd2.jardwd.space/)


# License
*nativeDroid2 is distributed under the MIT-License*

The MIT License (MIT)

Copyright (c) 2018 Darren Young, JARD/BisDev / http://bisdev.space / http://jard.space

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



